using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Prendi_la_talpa
{
    /// <summary>
    /// Logica di interazione per MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Button b = new Button();
        DispatcherTimer time1 = new DispatcherTimer();
        double i = 500;// prova 1
        public MainWindow()
        {
            Grid miogrid = new Grid();
            miogrid.Background = Brushes.Red;
            for(int i = 0; i < 10; i++)
            {
                miogrid.ColumnDefinitions.Add(new ColumnDefinition());
                miogrid.Background = Brushes.Black;
            }
            for(int i = 0; i < 10; i++)
            {
                miogrid.RowDefinitions.Add(new RowDefinition());
                miogrid.Background = Brushes.Black;
            }
            miogrid.ShowGridLines=true;
            time1.Interval = TimeSpan.FromMilliseconds(i);
            time1.Tick += Time1_Tick;
            miogrid.Children.Add(b);


            InitializeComponent();
            mi.AddChild(miogrid);
            time1.Start();
            b.Click += B_Click;
        }

        private void B_Click(object sender, RoutedEventArgs e)
        {
           i= i *0.5;
        }

        private void Time1_Tick(object sender, EventArgs e)
        {
            Random r = new Random();

           int righe= r.Next(0, 9);
            int colonne = r.Next(0, 9);
            Grid.SetColumn(b, colonne);
            Grid.SetRow(b, righe);
        }

    }
}
